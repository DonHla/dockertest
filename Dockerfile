FROM python:3-slim

WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt && \
    chmod +x /app/init.sh

CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000
